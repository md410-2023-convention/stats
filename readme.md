# Lions MD410 2023 Oceans of Opportunity Convention Stats Server

A [Flask](https://palletsprojects.com/p/flask/)-based server to provide registree stats for the Lions MD410 2023 Oceans of Opportunity Convention.

# Operation

The server is most easily used as a Docker image: registry.gitlab.com/md410-2023-convention/stats:main. It requires several environment variables:

* **MONGODB_HOST**: Host of the mongodb server holding registration data
* **MONGODB_PORT**: Port of the mongodb server holding registration data
* **MONGODB_USERNAME**: Username for the mongodb server holding registration data
* **MONGODB_PASSWORD**: Passord for the mongodb server holding registration data
