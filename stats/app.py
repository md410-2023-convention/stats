from flask import Flask, render_template, send_file, redirect, url_for

from datetime import datetime
from operator import itemgetter

import mongo

app = Flask(__name__)


@app.route("/")
def stats():
    stats = mongo.populate()
    return render_template(
        "stats.html",
        stats=stats,
        dt=f"{datetime.now():%H:%M on %Y/%m/%d}",
    )


# @app.route("/public")
# def public_stats():
#     stats = mongo.Stats()
#     stats.populate(mongo.collection)
#     attendees = [(a.last_name.lower(), a) for a in stats.attendees]
#     attendees.sort(key=itemgetter(0))
#     return render_template(
#         "public_stats.html",
#         stats=stats,
#         attendees=attendees,
#         dt=f"{datetime.now():%H:%M on %Y/%m/%d}",
#     )


@app.route("/stats_file")
def excel_file():
    stats = mongo.populate()
    fn = "md_convention_2023_stats.xlsx"
    stats.make_excel(f"/tmp/{fn}")
    return send_file(f"/tmp/{fn}", as_attachment=True, download_name=fn)
