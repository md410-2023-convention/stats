from collections import Counter, defaultdict
from datetime import datetime
import os

import clubs
import excel

from attrs import asdict, define
from dotenv import load_dotenv
from pymongo import MongoClient
from rich import print

load_dotenv()

client = MongoClient(
    host=os.getenv("MONGODB_HOST"),
    port=int(os.getenv("MONGODB_PORT")),
    username=os.getenv("MONGODB_USERNAME"),
    password=os.getenv("MONGODB_PASSWORD"),
)
db = client["md410_2023_conv"]
reg_collection = db["reg_form"]
partial_reg_collection = db["partial_reg_form"]
payment_collection = db["payment"]
partial_payment_collection = db["partial_payment"]


@define
class Attendee:
    attendee_num: int
    name: str
    name_badge: str
    club: str
    district: str
    pins: int
    dietary: str
    disability: str
    cell: str
    email: str
    lion: bool
    mjf_lunch: str
    pdg_dinner: str
    beach_cleanup: str
    lpe_breakfast: str
    partner_program: str
    full_reg: str = "Yes"
    welcome: str = "Yes"
    dist_conv: str = "Yes"
    banquet: str = "Yes"
    md_conv: str = "Yes"
    theme: str = "Yes"
    owed: float = 0
    paid: float = 0
    first_names: str = ""
    last_name: str = ""


@define
class Stats:
    total_attendees: int = 0
    lions: int = 0
    non_lions: int = 0
    mjf_lunch: int = 0
    pdg_dinner: int = 0
    beach_cleanup: int = 0
    lpe_breakfast: int = 0
    partner_program: int = 0
    welcome: int = 0
    dist_e_conv: int = 0
    dist_w_conv: int = 0
    dist_x_conv: int = 0
    banquet: int = 0
    md_conv: int = 0
    theme: int = 0
    pins: int = 0
    clubs: Counter = None
    districts: Counter = None
    dietary: list = None
    disability: list = None
    full_attendees: list = None
    partial_attendees: list = None
    attendees: list = None

    def populate(
        self, reg_collection=None, partial_reg_collection=None, payment_collection=None
    ):
        reg_record_sets = {
            "full": list(reg_collection.find()),
            "partial": list(partial_reg_collection.find()),
        }
        self.clubs = Counter()
        self.districts = Counter()
        self.dietary = []
        self.disability = []
        self.full_attendees = []
        self.partial_attendees = []
        for set_type, reg_records in reg_record_sets.items():
            for reg_record in (r for r in reg_records if not r.get("deleted", False)):
                self.pins += int(reg_record["items"]["pins"])
                for attendee_num, attendee in enumerate(
                    reg_record["attendees"]
                    if set_type == "full"
                    else [reg_record["attendee"]],
                    1,
                ):
                    locals = {}
                    if attendee["lion"]:
                        self.lions += 1
                        locals["lion"] = True
                    else:
                        self.non_lions += 1
                        locals["lion"] = False
                    attrs = [
                        "mjf_lunch",
                        "pdg_dinner",
                        "beach_cleanup",
                        "lpe_breakfast",
                    ]
                    if set_type == "full":
                        attrs.append("partner_program")
                    for attr in attrs:
                        if attendee[attr]:
                            setattr(self, attr, getattr(self, attr) + 1)
                            locals[attr] = True
                        else:
                            locals[attr] = False
                    self.clubs[attendee["club"]] += 1
                    district = clubs.CLUBS.get(attendee["club"])
                    self.districts[district] += 1
                    key = f"dist_{district[-1].lower() if district else 'x'}_conv"
                    setattr(self, key, getattr(self, key) + 1)
                    for attr in "welcome banquet md_conv theme".split(" "):
                        if set_type == "full":
                            inc = 1
                        else:
                            inc = reg_record["items"].get(attr, 0)
                        setattr(self, attr, getattr(self, attr) + inc)
                    for attr in ("dietary", "disability"):
                        if attendee[attr] and (
                            attendee[attr].lower().strip()
                            not in (
                                "none",
                                "mone",
                                "normal",
                                "no",
                                "non",
                                "n/a",
                                "na",
                                "nil",
                                "not applicable",
                                "non veg",
                                "i eat all",
                            )
                        ):
                            getattr(self, attr).append(attendee[attr])
                            locals[attr] = attendee[attr]
                    if (set_type == "full" and attendee_num == 1) or (
                        set_type == "partial"
                    ):
                        owed = reg_record["cost"]
                        q = {
                            "full": payment_collection,
                            "partial": partial_payment_collection,
                        }[set_type].find({"reg_num": reg_record["reg_num"]})
                        paid = sum(p["amount"] for p in q)
                    else:
                        owed = None
                        paid = None
                    if set_type == "full":
                        self.full_attendees.append(
                            Attendee(
                                f"{reg_record['reg_num']:03}/{attendee['attendee_num']}",
                                attendee["full_name"],
                                attendee["name_badge"],
                                attendee["club"],
                                clubs.CLUBS.get(attendee["club"]),
                                int(reg_record["items"]["pins"])
                                if (attendee["attendee_num"] == 1)
                                else 0,
                                locals.get("dietary", "None"),
                                locals.get("disability", "None"),
                                attendee.get("cell", ""),
                                attendee["email"],
                                "Yes" if attendee["lion"] else "No",
                                "Yes" if bool(attendee["mjf_lunch"]) else "No",
                                "Yes" if bool(attendee["pdg_dinner"]) else "No",
                                "Yes" if bool(attendee["beach_cleanup"]) else "No",
                                "Yes" if bool(attendee["lpe_breakfast"]) else "No",
                                "Yes" if bool(attendee["partner_program"]) else "No",
                                "Yes",
                                "Yes",
                                "Yes",
                                "Yes",
                                "Yes",
                                "Yes",
                                owed,
                                paid,
                                attendee["first_names"],
                                attendee["last_name"],
                            )
                        )
                    else:
                        self.partial_attendees.append(
                            Attendee(
                                reg_record["reg_num_string"],
                                attendee["full_name"],
                                attendee["name_badge"],
                                attendee["club"],
                                clubs.CLUBS.get(attendee["club"]),
                                int(reg_record["items"]["pins"]),
                                locals.get("dietary", "None"),
                                locals.get("disability", "None"),
                                attendee.get("cell", ""),
                                attendee["email"],
                                "Yes" if attendee["lion"] else "No",
                                "Yes" if bool(attendee["mjf_lunch"]) else "No",
                                "Yes" if bool(attendee["pdg_dinner"]) else "No",
                                "Yes" if bool(attendee["beach_cleanup"]) else "No",
                                "Yes" if bool(attendee["lpe_breakfast"]) else "No",
                                "No",
                                "No",
                                "Yes" if reg_record["items"].get("welcome") else "No",
                                "Yes" if reg_record["items"].get("dist_conv") else "No",
                                "Yes" if reg_record["items"].get("banquet") else "No",
                                "Yes" if reg_record["items"].get("md_conv") else "No",
                                "Yes" if reg_record["items"].get("theme") else "No",
                                owed,
                                paid,
                                attendee["first_names"],
                                attendee["last_name"],
                            )
                        )

        self.attendees = self.full_attendees + self.partial_attendees
        self.total_attendees = self.lions + self.non_lions
        for set_type, reg_records in reg_record_sets.items():
            for reg_record in (r for r in reg_records if not r.get("deleted", False)):
                if (
                    reg_record["attendees"][0]
                    if set_type == "full"
                    else reg_record["attendee"]
                )["club"] not in clubs.CLUBS:
                    print(f"Unspecified District: {reg_record}")
        self.dietary = list(set(self.dietary))
        self.disability = list(set(self.disability))

    def make_excel(self, file_name="md_convention_2023_stats.xlsx"):
        wb = excel.ExcelWorkbook(datetime.now(), file_name)
        districts = defaultdict(list)
        for attendee in self.attendees:
            districts["All"].append(asdict(attendee).values())
            districts[attendee.district].append(asdict(attendee).values())
        for title, rows in list(districts.items()):
            if title is not None:
                sheet_name = f"{title} Attendees"
            else:
                sheet_name = "Unspecified District Attendees"
            wb.add_registrees_sheet(
                sheet_name,
                [
                    "Attendee Num",
                    "Name",
                    "Name Badge",
                    "Club",
                    "District",
                    "Number of Pins",
                    "Dietary Requirements",
                    "Disability Requirements",
                    "Cell Phone",
                    "Email",
                    "Lion",
                    "MJF Lunch",
                    "PDG Dinner",
                    "Beach Cleanup",
                    "LPE Breakfast",
                    "Partner Program",
                    "Full Registration",
                    "Welcome Evening",
                    "District Convention",
                    "Banquet",
                    "MD Convention",
                    "Theme Evening",
                ],
                widths={
                    "Attendee Num": 20,
                    "Name": 40,
                    "Name Badge": 40,
                    "Club": 30,
                    "District": 15,
                    "Dietary Requirements": 50,
                    "Disability Requirements": 50,
                    "Cell Phone": 30,
                    "Email": 30,
                },
            )
            for row in rows:
                wb.add_row(list(row)[:-4])

        wb.add_registrees_sheet(
            "Totals", ["Item", "Total"], widths={"Item": 30, "Total": 20}
        )
        wb.add_row(["Total Attendees", self.total_attendees])
        wb.add_row(["Lions", self.lions])
        wb.add_row(["Non-Lions", self.non_lions])
        wb.add_row(["MJF Lunch", self.mjf_lunch])
        wb.add_row(["PDG Dinner", self.pdg_dinner])
        wb.add_row(["Beach Cleanup", self.beach_cleanup])
        wb.add_row(["President's Elect Breakfast", self.lpe_breakfast])
        wb.add_row(["Partner's Program", self.partner_program])
        wb.add_row(["Pins", self.pins])
        wb.add_row(["Welcome Evening", self.welcome])
        wb.add_row(["District E Convention", self.dist_e_conv])
        wb.add_row(["District W Convention", self.dist_w_conv])
        wb.add_row(["Banquet", self.banquet])
        wb.add_row(["MD Convention", self.md_conv])
        wb.add_row(["Theme Evening", self.theme])

        wb.add_list_sheet(
            "Registrees by reg num",
            [
                "Reg Num",
                "Name",
                "# of Pins",
                "Signature – registered\nand gift bag received\n(if applicable)",
            ],
            widths={
                "Reg Num": 10,
                "Name": 35,
                "# of Pins": 20,
                "Signature – registered\nand gift bag received\n(if applicable)": 35,
            },
        )
        for attendee in self.attendees:
            wb.add_row([attendee.attendee_num, attendee.name, attendee.pins, ""])
        wb.save()


def populate():
    stats = Stats()
    stats.populate(reg_collection, partial_reg_collection, payment_collection)
    return stats


if __name__ == "__main__":
    stats = populate()
    # print(stats.attendees[-4:-3])
    if 0:
        for attr in (
            "total_attendees",
            "lions",
            "non_lions",
            "mjf_lunch",
            "pdg_dinner",
            "beach_cleanup",
            "lpe_breakfast",
            "partner_program",
            "welcome",
            "dist_e_conv",
            "dist_w_conv",
            "dist_x_conv",
            "banquet",
            "md_conv",
            "theme",
            "pins",
            "clubs",
            "districts",
            "dietary",
            "disability",
        ):
            print(getattr(stats, attr))
    stats.make_excel()
    # print(
    #     ";".join(
    #         [
    #             attendee.email
    #             for attendee in stats.attendees
    #             if attendee.district == "410E"
    #         ]
    #     )
    # )
    # print(stats)
